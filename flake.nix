{
  description = ''
    NixOS Rebuild Flake
      | includes       
      * Home-Manager
      * Grub2-Themes
  '';
  inputs = {
    hosts.url = "github:StevenBlack/hosts";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.11";
    home-manager = {
      url = github:nix-community/home-manager;
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    home-manager,
    hosts,
    ...
  }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        config.allowUnfree = true;
      };
      lib = nixpkgs.lib;
    in {
      nixosConfigurations = {
        nixos = lib.nixosSystem { # "nixos" can be any hostname
          inherit system;
          modules = [
            ./mac/configuration.nix
            home-manager.nixosModules.home-manager {
              home-manager = {
                useGlobalPkgs = true;
                useUserPackages = true;
                users = {
                  mac = { pkgs, ... }: {
                    imports = [
                      ./mac/home.nix
                    ];
                  };
                };
              };
            }
            hosts.nixosModule {
              networking.stevenBlackHosts.enable = true;
            }
            # end home-manager module block
          ];
        };
      };
    };
}
