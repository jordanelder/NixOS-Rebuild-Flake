{ config, pkgs, lib, modulesPath, ... }: {
####################################################################
## The eventual new structure will follow this line of imports
## lines with "-o-" are currently outlined in this config
## lines with "-x-" still need split from the "services.nix" block
####################################################################
#   imports = [
#   -o- filesystem.nix
#   -o- boot.nix
#   -o- os.nix
#   -o- sound-hardware.nix
#   -o- time-networking.nix
#   -o- services.nix
#   -o- fonts.nix
#   -o- overlays.nix
#   -o- packages.nix
#   -o- users.nix
#   -o intel.nix
#   -o intel-override.nix
#
#   -x- X.nix
#   -x- xmonad.nix
#   -x- plasma.nix
#   -x- gnome.nix

#   -x- vm.nix
  #   ];
  ####################################################################
  # --- os.nix
  # --- ^^^^^^
  # --- * imports
  # --- * nix
  # --- * nixpkgs
  # --- * enable "Nix Flakes" an experimental feature
  # ### ======================
  # 
  system.stateVersion = "21.11";
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    ./filesystem.nix
    ./boot.nix
    # ./OS.nix
    ./sound-hardware.nix
    ./networking.nix
    # ./services.nix
    ./fonts.nix
    ./overlays.nix
    ./tmux.nix
    # ./packages.nix
    ./users.nix
    ./intel.nix
    ./java.nix
    ./lidswitch.nix
    ./lidswitch-docked.nix
    ./virtualisation.nix
  ];
  nix = { 
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };
  nixpkgs = {
    config = {
      allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
        "steam-runtime"
        "vscode"
      ];
    };
  };


  # ### ======================
  # --- services.nix
  # --- ^^^^^^^^^^^^
  # --- * start xserver with xmonad, plasma enabled, lightdm, and seemingly no background
  # --- * default is xmonad
  # --- * enable plasma
  # --- * lightdm (background not working)
  # --- * lorri is a dev enhanced nix-shell
  # --- * ssh needs work before it can be used
  # --- * add hpkgs
  # --- * enso greeter also seems like the theme isn't working, this must be something to do with direnv pathing
  # --- * session commands feh to set wallpaper seems pretty hacky right now
  # --- * session commands uxvrt and xterm sane defaults
  # --- *  this needs futher broken down into "X.nix" and "xmonad.nix" and "plasma.nix" and "intel.nix"
  #
  services = {
    lorri.enable = true;
    xserver = {
      enable = true;
      autorun = true;
      layout = "us,no";
      # videoDrivers = [ "intel" ]; # want to declare in "intel.nix"
      windowManager = {
        xmonad = {
          enable = true;
          enableContribAndExtras = true;
          extraPackages = hpkgs: [
            hpkgs.xmonad-contrib
            hpkgs.xmonad-extras
            hpkgs.xmobar
            hpkgs.xmonad-screenshot
          ];
          # config = "xmonad.hs";
        };
      };
      desktopManager.plasma5.enable = true;
      displayManager = {
        defaultSession = "none+xmonad";
        lightdm = {
          # greeters.mini = {
          #   enable = true;
          #   user = "mac";
          #   extraConfig = ''
          #     [greeter]
          #     show-password-label = false
          #     [greeter-theme]
          #     background-image = "/home/mac/Desktop/laptop-nix-1.png"
          #   '';
          # };
          # greeters.pantheon.enable = true;
          # background = "/home/mac/Desktop/laptop-nix-1.png";
          greeters.enso = {
            enable = true;
            brightness = 7;
            blur = false;
          };
        };
        sessionCommands =  ''
          ${pkgs.feh}/bin/feh --bg-scale /home/mac/Documents/mac/wallpaper.png;
          xrdb "${pkgs.writeText "xrdb.conf" ''
            URxvt.font:                 xft:FiraMono:style=Regular:pixelsize=16
            URxvt.boldFont:             xft:FiraMono:style=Bold:pixel:pixelsize=16
            URxvt.italicFont:           xft:FiraMono:style=Light:pixel:pixelsize=16
            XTerm*utf8:                 2
            URxvt.letterSpace:          0
            URxvt.background:           #121214
            URxvt.foreground:           #FFFFFF
            XTerm*background:           #121212
            XTerm*foreground:           #FFFFFF
            ! black
            URxvt.color0  :             #2E3436
            URxvt.color8  :             #555753
            XTerm*color0  :             #2E3436
            XTerm*color8  :             #555753
            ! red
            URxvt.color1  :             #CC0000
            URxvt.color9  :             #EF2929
            XTerm*color1  :             #CC0000
            XTerm*color9  :             #EF2929
            ! green
            URxvt.color2  :             #4E9A06
            URxvt.color10 :             #8AE234
            XTerm*color2  :             #4E9A06
            XTerm*color10 :             #8AE234
            ! yellow
            URxvt.color3  :             #C4A000
            URxvt.color11 :             #FCE94F
            XTerm*color3  :             #C4A000
            XTerm*color11 :             #FCE94F
            ! blue
            URxvt.color4  :             #3465A4
            URxvt.color12 :             #729FCF
            XTerm*color4  :             #3465A4
            XTerm*color12 :             #729FCF
            ! magenta
            URxvt.color5  :             #75507B
            URxvt.color13 :             #AD7FA8
            XTerm*color5  :             #75507B
            XTerm*color13 :             #AD7FA8
            ! cyan
            URxvt.color6  :             #06989A
            URxvt.color14 :             #34E2E2
            XTerm*color6  :             #06989A
            XTerm*color14 :             #34E2E2
            ! white
            URxvt.color7  :             #D3D7CF
            URxvt.color15 :             #EEEEEC
            XTerm*color7  :             #D3D7CF
            XTerm*color15 :             #EEEEEC
            URxvt*saveLines:            32767
            XTerm*saveLines:            32767
            URxvt.colorUL:              #AED210
            Xft*dpi:                    96
            Xft*antialias:              true
            Xft*hinting:                full
            URxvt.scrollBar:            false
            URxvt*scrollTtyKeypress:    true
            URxvt*scrollTtyOutput:      false
            URxvt*scrollWithBuffer:     false
            URxvt*scrollstyle:          plain
            URxvt*secondaryScroll:      true
            Xft.autohint: 1
            Xft.lcdfilter:  lcddefault
            Xft.hintstyle:  hintfull
            Xft.hinting: 1
            Xft.antialias: 1 
         ''}"
       '';
      };
    };
    nginx = {
      enable = true;
      # recommendedProxySettings = true;
      # recommendedTlsSettings = true;
      # virtualHosts."localhost" = {
      #   locations."/var/www/".proxyPass = "http://localhost:8000";
      # };
      # appendHttpConfig = "listen 127.0.0.1:80";
      # --- working
      virtualHosts."jmac217.home" = {
        enableACME = false;
        forceSSL = false;
        root = "/var/www/landing.page";
      };
      # --- 
      # recommendedGzipSettings = true;
      # recommendedOptimisation = true;
      # recommendedProxySettings = true;
      # recommendedTlsSettings = true;
      # sslCiphers = "AES256+EECDH:AES256+EDH:!aNULL";
      # commonHttpConfig = ''
      # # Add HSTS header with preloading to HTTPS requests.
      # # Adding this header to HTTP requests is discouraged
      # map $scheme $hsts_header {
      #     https   "max-age=31536000; includeSubdomains; preload";
      # }
      # add_header Strict-Transport-Security $hsts_header;
      # 
      # # Enable CSP for your services.
      # #add_header Content-Security-Policy "script-src 'self'; object-src 'none'; base-uri 'none';" always;

      # # Minimize information leaked to other domains
      # add_header 'Referrer-Policy' 'origin-when-cross-origin';

      # # Disable embedding as a frame
      # add_header X-Frame-Options DENY;

      # # Prevent injection of code in other mime types (XSS Attacks)
      # add_header X-Content-Type-Options nosniff;

      # # Enable XSS protection of the browser.
      # # May be unnecessary when CSP is configured properly (see above)
      # add_header X-XSS-Protection "1; mode=block";

      # # This might create errors
      # proxy_cookie_path / "/; secure; HttpOnly; SameSite=strict";
      # '';
      # virtualHosts = let
      #   base = locations: {
      #    inherit locations;
      #     addSSL = false;
      #     forceSSL = false
      #     enableACME = false;
      #     # root = "/var/www/langing.page";
      #   };
      #   proxy = port: base {
      #     "/".proxyPass = "http://127.0.0.1:" + toString(port) + "/";
      #   };
      #   in {
      #     "landing.page" = proxy 3000 // { default = true; };
      #   };
        # add for php
        # locations."~ \.php$".extraConfig = ''
        #   fastcgi_pass unix:${config.services.phpfpm.pools.mypool.socket};
        #   fastcgi_index index.php;
        # '';
        # add if logging is required
        # systemd.services.nginx.serviceConfig.ReadWritePaths = [ "/var/spool/nginx/logs/" ];
      # };
    };
    # enable mysql
    # mysql = {
    #   enable = true;
    #   package = pkgs.mariadb;
    # };
    # enable phpfpm cgi_mod stuff
    # phpfpm.pools.mypool = {
    #   user = "nobody";
    #   settings = {
    #     pm = "dynamic";
    #     "listen.owner" = config.services.nginx.user;
    #     "pm.max_children" = 5;
    #     "pm.start_servers" = 2;
    #     "pm.min_spare_servers" = 1;
    #     "pm.max_spare_servers" = 3;
    #     "pm.max_requests" = 500;
    #   };
    # };
  };
  # security.acme.certs = {
  #   "langing.page".email = "jordanelder10@gmail.com";
  # };
  # ### ======================
  # --- packages.nix
  # --- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  # --- * install tons of stuff
  # --- * this list needs divided up into sections
  #
  environment = {
    systemPackages = with pkgs; [
      vim                      # Text Editor
      wget                     # Download Tool
      firefox                  # Userland Browser
      dmenu                    # A menu for use with xmonad
      feh                      # A light-weight image viewer to set backgrounds
      haskellPackages.libmpd   # Shows MPD status in xmobar
      haskellPackages.xmobar   # A Minimalistic Text Based Status Bar
      libnotify                # Notification client for my Xmonad setup
      lxqt.lxqt-notificationd  # The notify daemon itself
      mpc_cli                  # CLI for MPD, called from xmonad
      scrot                    # CLI screen capture utility
      trayer                   # A system tray for use with xmonad
      xbrightness              # X11 brigthness and gamma software control
      xcompmgr                 # X composting manager
      xorg.xrandr              # CLI to X11 RandR extension
      xscreensaver             # My preferred screensaver
      xsettingsd               # A lightweight desktop settings server
      git
      ripgrep
      coreutils
      fd
      clang
      gparted
      nixos-generators
      kde-gtk-config
      ffmpeg
      cmatrix
      variety
      pipes
      sl
      lsd
      rxvt-unicode
      alacritty
      roxterm
      xterm
      terminator
      qutebrowser
      aha
      html2text
      yt-dlp
      gimp
      bottles
      emacs
      mpv
      vscode
      breeze-grub
      direnv
      neofetch
      filezilla
      gcsfuse
      google-drive-ocamlfuse
      unzip
      qemu_full
      qemu-utils
      quickemu
      # My apps
    ];
  };
}
