{ config, lib, pkgs, options, modulesPath, vars, ... }:{
  environment.systemPackages = with pkgs; [
    jre8
    jdk8
  ];
}
