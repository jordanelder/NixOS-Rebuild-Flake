{ config, pkgs, lib, ... }:
{
  programs.tmux = {
    enable = true;
    terminal = "xterm-256color";
    clock24 = true;
    extraConfig = builtins.readFile (
      builtins.fetchurl {
        url = https://raw.githubusercontent.com/gpakosz/.tmux/master/.tmux.conf;
        sha256 = "0ihzgvwfpqm7hjdmr7qi38fkpyclqawzcp4drfy0k3hgwavj7lgc";
      }
    );
    extraTmuxConf = builtins.readFile (
      builtins.fetchurl {
        url = https://raw.githubusercontent.com/gpakosz/.tmux/master/.tmux.conf.local;
        sha256 = "1d3523gjj4xxfchvj9mbkzymblw5xgly74f0k56sjrqcbknl3ln3";
      }
    );
  };
}