{ config, pkgs, lib, modulesPath, ... }: {


  ####################################################################
  ## The eventual new structure will follow this line of imports
  ## lines with "-o-" are currently outlined in this config
  ## lines with "-x-" still need split from the "services.nix" block
  ####################################################################
  #   imports = [
  #   -o- nixpkgs.config.nix
  #   -o- overlays.nix
  #   -o- time-networking.nix
  #   -o- fonts.nix
  #   -o- sound-hardware.nix
  #   -o- users.nix
  #   -o- environment.systemPackages.nix
  #   -o- services.nix
  #   -o- boot.nix
  #   -x- sys.nix
  #   -x- X.nix
  #   -x- xmonad.nix
  #   -x- plasma.nix
  #   -x- gnome.nix
  #   -x- intel.nix
  #   -x- vm.nix
  #   ];
  ####################################################################
  # 
  # ### ======================
  # --- os.nix
  # --- ^^^^^^
  # --- * imports
  # --- * nix
  # --- * nixpkgs
  # --- * enable "Nix Flakes" an experimental feature
  # ### ======================
  # 
  system.stateVersion = "21.11";
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];
  nix = { 
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };
  nixpkgs = { 
    config = {
      allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
        "steam-runtime"
        "vscode"
      ];
    };
  # 
  # ### ======================
  # --- filesystem.nix
  # --- ^^^^^^^^^^^^^^
  # --- * sort out block devices
  # ### ======================
  # 
  fileSystems."/" =
    {
      device = "/dev/disk/by-label/nixos";
      fsType = "ext4";
    };
  swapDevices = [
    {
      device = "/dev/disk/by-label/swap";
    }
  ];
  # 
  # ### ======================
  # --- boot.nix
  # --- ^^^^^^^^
  # --- * clean tmp dir and use tmpfs (ram /tmp instead of disk)
  # --- * plymouth block (theme doesn't work yet)
  # --- * enable ntfs
  # --- * set grub on /dev/sdB
  # --- * use OSProber
  # --- * customization options
  # --- * "extraEntries" block (theme doesn't work yet)
  # --- * modules ["uhci_hcd" "ehci_pci" "ahci" "usb_storage" "sd_mod"]
  # ### ======================
  # 
  boot = { 
    cleanTmpDir = true;
    tmpOnTmpfs = true;
    # plymouth = {
    #   enable = true;
    #   themePackages = [ pkgs.adi1090x-plymouth ];
    #   theme = "lone";
    # };
    loader.grub = { 
      enable = true;
      version = 2;
      device = "/dev/sdb";
      useOSProber = true;
      # backgroundColor = lib.mkForce "#09090B";
      # font = "/home/mac/.local/share/fonts/fontawesome.ttf";
      # fontSize = 20;
      # extraConfig = ''
      #   set theme=${pkgs.breeze-grub}/grub/themes/breeze/theme.txt
      # '';
      # splashImage = null;
      # extraEntries = ''
      #   menuentry "Windows 10" {
      #     chainloader (hd1,1)+1
      #   }
      # '';
    };
    supportedFilesystems = [ "ntfs" ];
    kernelModules = [ ];
    extraModulePackages = [ ];
    initrd = {
      availableKernelModules = [ "uhci_hcd" "ehci_pci" "ahci" "usb_storage" "sd_mod" ];
      kernelModules = [ ];
    };
  };
  # 
  # ### ======================
  # --- intel.nix
  # --- ^^^^^^^^^
  # --- * activate the normal intel thing
  # --- * want to break out the intel video driver but doesn't work
  # 
  hardware.cpu.intel.updateMicrocode = true;
  # services.xserver.videoDrivers = [ "intel" ];
  # 
  # ### ======================
  # --- intel-override.nix
  # --- ^^^^^^^^^^^^^^^^^^ 
  # --- * use vaapiIntel hybridCodec
  # --- * activate redistributable firmware thing
  # --- * add all kinds of intel stuff with extraPackages
  #
  # nixpkgs.packageOverrides = pkgs: {
  #   vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
  # };
  # hardware = {
  #   cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
  #   extraPackages = with pkgs; [
  #     intel-media-driver # LIBVA_DRIVER_NAME=iHD
  #     vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
  #     vaapiVdpau
  #     libvdpau-va-gl
  #   ];
  # };
  #
  # ### ======================
  # --- overlays.nix
  # --- ^^^^^^^^^^^^
  # --- * mpris is mpv but better sane defaults
  #
    overlays = [
      (self: super: {
        mpv = super.mpv-with-scripts.override {
          scripts = [ self.mpvScripts.mpris ];
        };
      })
      # (import ./packages)
    ];
  };
  #
  # ### ======================
  # --- time-networking.nix
  # --- ^^^^^^^^^^^^^^^^^^^
  # --- * time
  # --- * hostname
  # --- * network devices
  # --- * networkmanager
  #
  time.timeZone = "America/Chicago";
  networking.hostName = "nixos";
  networking = {
    hostName = "nixos";
    useDHCP = false;
    interfaces.enp3s0.useDHCP = true;
    networkmanager = {
      enable=true;
    };
  };
  #
  # ### ======================
  # --- fonts.nix
  # --- ^^^^^^^^^
  # --- * add all the fonts listed in the wiki
  # --- * override fonts with nerdfonts (trying to get ligature support in emacs with firacode)
  #
  fonts.fonts = with pkgs; [
    (nerdfonts.override { fonts = [ "FiraCode" "FiraMono" ]; })
    noto-fonts
    noto-fonts-cjk
    noto-fonts-emoji
    liberation_ttf
    fira-code
    fira-code-symbols
    mplus-outline-fonts
    dina-font
    proggyfonts
  ];
  #
  # ### ======================
  # --- sound-hardware.nix
  # --- ^^^^^^^^^^^^^^^^^^
  # --- * pulseaudio
  # --- * opengl (doesn't work on my laptop and I tried everything before realizing that)
  # --- * intel is declared above but I want it to be a part of this nix module if possible
  #
  sound.enable = true;
  hardware = {
    # cpu.intel.updateMicrocode = true; # declared above
    pulseaudio.enable = true;
    opengl = {
      enable = true;
      };
  };
  #
  # ### ======================
  # --- users.nix
  # --- ^^^^^^^^^
  # --- * add users
  # --- * initial password
  # --- * user to wheel + networkmanager
  #
  users.users.mac = {
    isNormalUser = true;
    initialPassword = "password";
    extraGroups = [ "wheel" "networkmanager" ];
  };
  #
  # ### ======================
  # --- environment.systemPackages.nix
  # --- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
  # --- * install tons of stuff
  # --- * this list needs divided up into sections
  #
  environment = {
    systemPackages = with pkgs; [
      vim                      # Text Editor
      wget                     # Download Tool
      firefox                  # Userland Browser
      dmenu                    # A menu for use with xmonad
      feh                      # A light-weight image viewer to set backgrounds
      haskellPackages.libmpd   # Shows MPD status in xmobar
      haskellPackages.xmobar   # A Minimalistic Text Based Status Bar
      libnotify                # Notification client for my Xmonad setup
      lxqt.lxqt-notificationd  # The notify daemon itself
      mpc_cli                  # CLI for MPD, called from xmonad
      scrot                    # CLI screen capture utility
      trayer                   # A system tray for use with xmonad
      xbrightness              # X11 brigthness and gamma software control
      xcompmgr                 # X composting manager
      xorg.xrandr              # CLI to X11 RandR extension
      xscreensaver             # My preferred screensaver
      xsettingsd               # A lightweight desktop settings server
      git
      ripgrep
      coreutils
      fd
      clang
      gparted
      nixos-generators
      kde-gtk-config
      ffmpeg
      tmux
      cmatrix
      variety
      pipes
      sl
      lsd
      rxvt-unicode
      alacritty
      roxterm
      xterm
      terminator
      qutebrowser
      aha
      html2text
      yt-dlp
      gimp
      bottles
      emacs
      mpv
      vscode
      breeze-grub
      direnv
      neofetch
      jre8
      jdk8_headless
      filezilla
      gcsfuse
      google-drive-ocamlfuse
      unzip
      # My apps
    ];
  };
  #
  # ### ======================
  # --- services.nix
  # --- ^^^^^^^^^^^^
  # --- * start xserver with xmonad, plasma enabled, lightdm, and seemingly no background
  # --- * default is xmonad
  # --- * enable plasma
  # --- * lightdm (background not working)
  # --- * lorri is a dev enhanced nix-shell
  # --- * ssh needs work before it can be used
  # --- * add hpkgs
  # --- * enso greeter also seems like the theme isn't working, this must be something to do with direnv pathing
  # --- * session commands feh to set wallpaper seems pretty hacky right now
  # --- * session commands uxvrt and xterm sane defaults
  # --- *  this needs futher broken down into "X.nix" and "xmonad.nix" and "plasma.nix" and "intel.nix"
  #
  # services.openssh.enable = true;
  # lorri
    services.lorri.enable = true;
  # openssh
    services.openssh = {
      enable = true;
      # passwordAuthentication = false;
      # permitRootLogin = "yes";
      # challengeResponseAuthentication = false;
    };
  # windowmanager
      services.xserver.windowManager = {
        xmonad = {
          enable = true;
          enableContribAndExtras = true;
          extraPackages = hpkgs: [
            hpkgs.xmonad-contrib
            hpkgs.xmonad-extras
            hpkgs.xmobar
            hpkgs.xmonad-screenshot
          ];
          # config = "xmonad.hs";
        };
      };


  # kde
      services.xserver.desktopManager.plasma5.enable = true;
  # displaymanager
      services.xserver.displayManager = {
        defaultSession = "none+xmonad";
        lightdm = {
          # greeters.mini = {
          #   enable = true;
          #   user = "mac";
          #   extraConfig = ''
          #     [greeter]
          #     show-password-label = false
          #     [greeter-theme]
          #     background-image = "/home/mac/Desktop/laptop-nix-1.png"
          #   '';
          # };
          # greeters.pantheon.enable = true;
          # background = "/home/mac/Desktop/laptop-nix-1.png";
          greeters.enso = {
            enable = true;
            brightness = 7;
            blur = false;
          };
        };

  # X
    services.xserver = {
      enable = true;
      autorun = true;
      layout = "us,no";
      # videoDrivers = [ "intel" ]; # want to declare in "intel.nix"
        sessionCommands =  ''
          ${pkgs.feh}/bin/feh --bg-scale /home/mac/Documents/mac/wallpaper.png;
          xrdb "${pkgs.writeText "xrdb.conf" ''
            URxvt.font:                 xft:FiraMono:style=Regular:pixelsize=16
            URxvt.boldFont:             xft:FiraMono:style=Bold:pixel:pixelsize=16
            URxvt.italicFont:           xft:FiraMono:style=Light:pixel:pixelsize=16
            XTerm*utf8:                 2
            URxvt.letterSpace:          0
            URxvt.background:           #121214
            URxvt.foreground:           #FFFFFF
            XTerm*background:           #121212
            XTerm*foreground:           #FFFFFF
            ! black
            URxvt.color0  :             #2E3436
            URxvt.color8  :             #555753
            XTerm*color0  :             #2E3436
            XTerm*color8  :             #555753
            ! red
            URxvt.color1  :             #CC0000
            URxvt.color9  :             #EF2929
            XTerm*color1  :             #CC0000
            XTerm*color9  :             #EF2929
            ! green
            URxvt.color2  :             #4E9A06
            URxvt.color10 :             #8AE234
            XTerm*color2  :             #4E9A06
            XTerm*color10 :             #8AE234
            ! yellow
            URxvt.color3  :             #C4A000
            URxvt.color11 :             #FCE94F
            XTerm*color3  :             #C4A000
            XTerm*color11 :             #FCE94F
            ! blue
            URxvt.color4  :             #3465A4
            URxvt.color12 :             #729FCF
            XTerm*color4  :             #3465A4
            XTerm*color12 :             #729FCF
            ! magenta
            URxvt.color5  :             #75507B
            URxvt.color13 :             #AD7FA8
            XTerm*color5  :             #75507B
            XTerm*color13 :             #AD7FA8
            ! cyan
            URxvt.color6  :             #06989A
            URxvt.color14 :             #34E2E2
            XTerm*color6  :             #06989A
            XTerm*color14 :             #34E2E2
            ! white
            URxvt.color7  :             #D3D7CF
            URxvt.color15 :             #EEEEEC
            XTerm*color7  :             #D3D7CF
            XTerm*color15 :             #EEEEEC
            URxvt*saveLines:            32767
            XTerm*saveLines:            32767
            URxvt.colorUL:              #AED210
            Xft*dpi:                    96
            Xft*antialias:              true
            Xft*hinting:                full
            URxvt.scrollBar:            false
            URxvt*scrollTtyKeypress:    true
            URxvt*scrollTtyOutput:      false
            URxvt*scrollWithBuffer:     false
            URxvt*scrollstyle:          plain
            URxvt*secondaryScroll:      true
            Xft.autohint: 1
            Xft.lcdfilter:  lcddefault
            Xft.hintstyle:  hintfull
            Xft.hinting: 1
            Xft.antialias: 1 
         ''}"
       '';
      };
    };


  services = {
  };
}
