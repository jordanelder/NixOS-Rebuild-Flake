####################################################################
## The eventual new structure will follow this line of imports
## lines with "-o-" are currently outlined in this config
## lines with "-x-" still need split from the "services.nix" block
####################################################################
#
#   -o- filesystem.nix
#   -o- boot.nix
#   -o- os.nix
#   -o- sound-hardware.nix
#   -o- time-networking.nix
#   -o- services.nix
#   -o- fonts.nix
#   -o- overlays.nix
#   -o- packages.nix
#   -o- users.nix
#   -o intel.nix
#   -o intel-override.nix
#
#   -x- X.nix
#   -x- xmonad.nix
#   -x- plasma.nix
#   -x- gnome.nix

#   -x- vm.nix
# 
# --- os.nix
# --- ^^^^^^
# --- * imports
# --- * nix
# --- * nixpkgs
# --- * enable "Nix Flakes" an experimental feature
# 
# --- filesystem.nix
# --- ^^^^^^^^^^^^^^
# --- * sort out block devices
# 
# --- boot.nix
# --- ^^^^^^^^
# --- * clean tmp dir and use tmpfs (ram /tmp instead of disk)
# --- * plymouth block (theme doesn't work yet)
# --- * enable ntfs
# --- * set grub on /dev/sdB
# --- * use OSProber
# --- * customization options
# --- * "extraEntries" block (theme doesn't work yet)
# --- * modules ["uhci_hcd" "ehci_pci" "ahci" "usb_storage" "sd_mod"]
# 
# --- intel.nix
# --- ^^^^^^^^^
# --- * activate the normal intel thing
# --- * want to break out the intel video driver but doesn't work
# 
# --- intel-override.nix
# --- ^^^^^^^^^^^^^^^^^^ 
# --- * use vaapiIntel hybridCodec
# --- * activate redistributable firmware thing
# --- * add all kinds of intel stuff with extraPackages
#
# --- overlays.nix
# --- ^^^^^^^^^^^^
# --- * mpris is mpv but better sane defaults
#
# --- time-networking.nix
# --- ^^^^^^^^^^^^^^^^^^^
# --- * time
# --- * hostname
# --- * network devices
# --- * networkmanager
#
# --- fonts.nix
# --- ^^^^^^^^^
# --- * add all the fonts listed in the wiki
# --- * override fonts with nerdfonts (trying to get ligature support in emacs with firacode)
#
# --- sound-hardware.nix
# --- ^^^^^^^^^^^^^^^^^^
# --- * pulseaudio
# --- * opengl (doesn't work on my laptop and I tried everything before realizing that)
# --- * intel is declared above but I want it to be a part of this nix module if possible
#
# --- users.nix
# --- ^^^^^^^^^
# --- * add users
# --- * initial password
# --- * user to wheel + networkmanager
#
# --- packages.nix
# --- ^^^^^^^^^^^^
# --- * install tons of stuff
# --- * this list needs divided up into sections
#
vim                      # Text Editor
wget                     # Download Tool
firefox                  # Userland Browser
dmenu                    # A menu for use with xmonad
feh                      # A light-weight image viewer to set backgrounds
haskellPackages.libmpd   # Shows MPD status in xmobar
haskellPackages.xmobar   # A Minimalistic Text Based Status Bar
libnotify                # Notification client for my Xmonad setup
lxqt.lxqt-notificationd  # The notify daemon itself
mpc_cli                  # CLI for MPD, called from xmonad
scrot                    # CLI screen capture utility
trayer                   # A system tray for use with xmonad
xbrightness              # X11 brigthness and gamma software control
xcompmgr                 # X composting manager
xorg.xrandr              # CLI to X11 RandR extension
xscreensaver             # My preferred screensaver
xsettingsd               # A lightweight desktop settings server
git
ripgrep
coreutils
fd
clang
gparted
nixos-generators
kde-gtk-config
ffmpeg
tmux
cmatrix
variety
pipes
sl
lsd
rxvt-unicode
alacritty
roxterm
xterm
terminator
qutebrowser
aha
html2text
yt-dlp
gimp
bottles
emacs
mpv
vscode
breeze-grub
direnv
neofetch
jre8
jdk8_headless
filezilla
gcsfuse
google-drive-ocamlfuse
unzip
#
# ### ======================
# --- services.nix
# --- ^^^^^^^^^^^^
# --- * start xserver with xmonad, plasma enabled, lightdm, and seemingly no background
# --- * default is xmonad
# --- * enable plasma
# --- * lightdm (background not working)
# --- * lorri is a dev enhanced nix-shell
# --- * ssh needs work before it can be used
# --- * add hpkgs
# --- * enso greeter also seems like the theme isn't working, this must be something to do with direnv pathing
# --- * session commands feh to set wallpaper seems pretty hacky right now
# --- * session commands uxvrt and xterm sane defaults
# --- *  this needs futher broken down into "X.nix" and "xmonad.nix" and "plasma.nix" and "intel.nix"
#
