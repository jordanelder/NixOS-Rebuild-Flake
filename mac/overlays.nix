{ config, pkgs, lib, vars, ... }: {
    #
    # ### ======================
    # --- overlays.nix
    # --- ^^^^^^^^^^^^
    # --- * mpris is mpv but better sane defaults
    #
    nixpkgs.overlays = [
        (self: super: {
            mpv = super.mpv-with-scripts.override {
            scripts = [ self.mpvScripts.mpris ];
            };
        })
        # (import ./packages)
    ];
}