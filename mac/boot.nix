{ config, pkgs, lib, modulesPath, ... }: {
  # ### ======================
  # --- boot.nix
  # --- ^^^^^^^^
  # --- * clean tmp dir and use tmpfs (ram /tmp instead of disk)
  # --- * plymouth block (theme doesn't work yet)
  # --- * enable ntfs
  # --- * set grub on /dev/sdB
  # --- * use OSProber
  # --- * customization options
  # --- * "extraEntries" block (theme doesn't work yet)
  # --- * modules ["uhci_hcd" "ehci_pci" "ahci" "usb_storage" "sd_mod"]
  # ### ======================
  # 
  boot = { 
    cleanTmpDir = true;
    tmpOnTmpfs = true;
    # plymouth = {
    #   enable = true;
    #   themePackages = [ pkgs.adi1090x-plymouth ];
    #   theme = "lone";
    # };
    loader.grub = { 
      enable = true;
      version = 2;
      device = "/dev/sdb";
      useOSProber = true;
      # backgroundColor = lib.mkForce "#09090B";
      # font = "/home/mac/.local/share/fonts/fontawesome.ttf";
      # fontSize = 20;
      # extraConfig = ''
      #   set theme=${pkgs.breeze-grub}/grub/themes/breeze/theme.txt
      # '';
      # splashImage = null;
      # extraEntries = ''
      #   menuentry "Windows 10" {
      #     chainloader (hd1,1)+1
      #   }
      # '';
    };
    supportedFilesystems = [ "ntfs" ];
    kernelModules = [ ];
    extraModulePackages = [ ];
    initrd = {
      availableKernelModules = [ "uhci_hcd" "ehci_pci" "ahci" "usb_storage" "sd_mod" ];
      kernelModules = [ ];
    };
  };
}