{ config, pkgs, lib, vars, ... }: {
    # ### ======================
    # --- sound-hardware.nix
    # --- ^^^^^^^^^^^^^^^^^^
    # --- * pulseaudio
    # --- * opengl (doesn't work on my laptop and I tried everything before realizing that)
    # --- * intel is declared above but I want it to be a part of this nix module if possible
    #
    sound.enable = true;
    hardware = {
    # cpu.intel.updateMicrocode = true; # declared above
    pulseaudio.enable = true;
    opengl = {
        enable = true;
        };
    };
    #
    #
}