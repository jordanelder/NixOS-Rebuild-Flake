{ config, pkgs, lib, vars, ... }: {
    # ### ======================
    # --- intel.nix
    # --- ^^^^^^^^^
    # --- * activate the normal intel thing
    # --- * want to break out the intel video driver but doesn't work
    # 
    hardware.cpu.intel.updateMicrocode = true;
    # services.xserver.videoDrivers = [ "intel" ];
    # 
    # ### ======================
    # --- intel-override.nix
    # --- ^^^^^^^^^^^^^^^^^^ 
    # --- * use vaapiIntel hybridCodec
    # --- * activate redistributable firmware thing
    # --- * add all kinds of intel stuff with extraPackages
    #
    # nixpkgs.packageOverrides = pkgs: {
    #   vaapiIntel = pkgs.vaapiIntel.override { enableHybridCodec = true; };
    # };
    # hardware = {
    #   cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
    #   extraPackages = with pkgs; [
    #     intel-media-driver # LIBVA_DRIVER_NAME=iHD
    #     vaapiIntel         # LIBVA_DRIVER_NAME=i965 (older but works better for Firefox/Chromium)
    #     vaapiVdpau
    #     libvdpau-va-gl
    #   ];
    # };
}