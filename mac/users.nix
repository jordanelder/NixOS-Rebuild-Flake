{ config, pkgs, lib, vars, ... }: {
    # ### ======================
    # --- users.nix
    # --- ^^^^^^^^^
    # --- * add users
    # --- * initial password
    # --- * user to wheel + networkmanager
    #
    users.users.mac = {
        isNormalUser = true;
        initialPassword = "password";
        extraGroups = [ "wheel" "networkmanager" "docker" ];
    };
}