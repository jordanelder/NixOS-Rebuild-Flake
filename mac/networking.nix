{ config, pkgs, lib, vars, hosts, ... }: {
    # ### ======================
    # --- networking.nix
    # --- ^^^^^^^^^^^^^^^^^^^
    # --- * time
    # --- * hostname
    # --- * network devices
    # --- * networkmanager
    #
    time.timeZone = "America/Chicago";
    services.openssh = {
        enable = true;
        openFirewall = true;
        passwordAuthentication = false;
        permitRootLogin = "no";
        authorizedKeysFiles = [
            "/home/mac/.ssh/nixos-laptop.ppk.pub"
        ];
        # challengeResponseAuthentication = false;
    };
    networking = {
        hostName = "nixos";
        useDHCP = false;
        interfaces.enp3s0.useDHCP = true;
        networkmanager = {
            enable=true;
        };
        firewall.extraCommands = ''
            iptables -A INPUT -p tcp --dport 25565 -j ACCEPT
            iptables -A INPUT -p udp --dport 25565 -j ACCEPT
            iptables -A INPUT -p tcp --dport 80 -j ACCEPT
            iptables -A INPUT -p udp --dport 80 -j ACCEPT
        '';

        stevenBlackHosts = {
            blockFakenews = true;
            blockGambling = true;
        };
    };
}