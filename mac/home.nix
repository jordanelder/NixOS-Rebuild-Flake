{ config, pkgs, ... }:

let
  username = "mac";
  homeDirectory = "/home/" + username;
  lat = 38.97;
  lon = -89.09;
  editor = "emacs";
in {
  # location.latitude = lat;
  # location.longitude = lon;
  # environment.variables.EDITOR = editor;
  home = {
    username = username;
    homeDirectory = homeDirectory;
    packages = with pkgs; [
      htop      
    ];
    stateVersion = "21.11";
  };
  programs.home-manager.enable = true;
  programs.nix-index.enable = true;
  programs.vim = {
    enable = true;
    plugins = with pkgs.vimPlugins; [
      vim-airline
      vim-git
      vim-nix
      vim-monokai-pro
      nginx-vim
      emmet-vim
    ];
    settings = { ignorecase = true; };
    extraConfig = ''
       set smartindent
       set autoindent
       set expandtab
       set shiftwidth=2
       set softtabstop=2
    '';
  };
}
