{   pkgs ? import <nixpkgs> {}
,   ...
}: {
    # ### ======================
    # --- virtualisation.nix
    # --- ^^^^^^^^^^^^^^^^^^^
    # --- * docker enable
    # --- * qemu options
    #
    virtualisation.docker.enable = true;
}