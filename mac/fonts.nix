{ config, pkgs, lib, vars, ... }: {
    # ### ======================
    # --- fonts.nix
    # --- ^^^^^^^^^
    # --- * add all the fonts listed in the wiki
    # --- * override fonts with nerdfonts (trying to get ligature support in emacs with firacode)
    #
    fonts.fonts = with pkgs; [
        (nerdfonts.override { fonts = [ "FiraCode" "FiraMono" ]; })
        noto-fonts
        noto-fonts-cjk
        noto-fonts-emoji
        liberation_ttf
        fira-code
        fira-code-symbols
        mplus-outline-fonts
        dina-font
        proggyfonts
    ];
}